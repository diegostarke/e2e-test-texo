class GetComponents {
  static get guideMenu() {
    return cy.get('a[href="/guide"]').contains('Guide');
  }
}

describe('Automating the scenario', () => {
  it('Access the link https://jsonplaceholder.typicode.com', () => {
    cy.visit('/');
  });

  it('Access the Guide menu', () => {
    GetComponents.guideMenu.click();
  });

  it('Access the link /albums/1/photos and capture data of photo with ID 6', () => {
    cy.request('/albums/1/photos')
      .then((response) => {
        expect(response.status).to.eq(200);
        const photosData = response.body;
        const photoId6 = photosData.find((photo) => photo.id === 6);

        expect(photoId6).to.exist;
        expect(photoId6.title).to.eq("accusamus ea aliquid et amet sequi nemo");
        expect(photoId6.url).to.eq("https://via.placeholder.com/600/56a8c2");
        expect(photoId6.thumbnailUrl).to.eq("https://via.placeholder.com/150/56a8c2");
      });
  });
});