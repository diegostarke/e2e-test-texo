Feature: Automating the scenario

  Scenario: Capturing and validating data of photo with ID 6
    Given I am on the JSONPlaceholder Guide page
    When I navigate to the Guide menu
    And I click on the "Albums" link
    And I open the photos of album with ID 1
    Then I capture the data of photo with ID 6
    And I validate the data of photo with ID 6