# Projeto de Testes E2E (Web) 

## Run Project
```bash
$ npm i

$ npm i -g ntl

$ ntl
```

## Run all tests 
* Select cypress:web

* Click E2E Testing

* Choose a Electron browser

* Click "Start E2E Testing in Electron"

* Click on app.cy.js file

## Report
On file app.feature is report